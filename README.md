# commoncrawl-use

This repository contains some code for extracting information about links to
web archives from the CommonCrawl web archive. They are used in some
experiments being conducted by Jess Ogden, Shawn Walker and Ed Summers to
examine the use of web archives on the web. 

Since there are many WARC files in each CommonCrawl snapshot and we don't
really want to reprocess them we download the WAT files for a snapshot and then
use link metadata included to determine when a given web page has linked to
a web archive. The list of domains we treat at web archives is included in the
`archives.json` file in this repository.

Once the processing is complete a CSV file is created with the following
columns:

* source_url: the url linking to the web archive
* source_host: the host name of the source url 
* archive_url: the URL of a web archive resource
* archive_service: the archive service, e.g. InternetArchive, ArchiveToday, etc
* link_text: the text of the hypertext link
* path: the CSS selector path to the link in the source page
* link_count: the total number of hyperlinks on the page
* warc: the CommonCrawl WARC file where the response is stored
* offset: the byte offset into the WARC file where the response is
* inflated_length: the inflated length of the response
* deflated_length: the compressed length of the response

## Install

You will need to install [Python3](https://python.org) and
[Redis](https://redis.io/) which is used to manage the work queue. Then you
need this repository and to install some Python dependencies. 

    git clone https://gitlab.com/slowa/commoncrawl-use.git
    cd commoncrawl-use
    pip install -r requirements.txt

## Run

Start Redis if it's not already running. Then you can fill up the work queue by
giving `enqueue.py` a snapshot ID of a [CommonCrawl] dataset. For example:

    ./enqueue.py CC-MAIN-2020-45

This will download the manifest of WAT files from CommonCrawl and add them to
a Redis queue.

Then you can start up as many workers as your computer can support.

    rq worker --logging_level INFO

Then look in the `data` directory for CSV files as they become available. There
should be a CSV file per WAT file.

Once the processing is complete you can aggregate all the individual CSV files
into one by running `bundle.py <snapshot_id>` which will create a file e.g.
`data/CC-MAIN-2020-45.csv`:

    ./bundle.py CC-MAIN-2020-45

If you want to run a lot of concurrent jobs you might want to manage the
workers with [superisord] using the supplied configuration file
`supervisord.conf`. However you will want to adjust the following configuration
parameters to use the appropriate paths:

* logfile
* childlogdir
* command
* directory

[CommonCrawl]: https://commoncrawl.org/
[CommonCrawl dataset]: https://commoncrawl.org/the-data/get-started/
[supervisord]: http://supervisord.org/logging.html
