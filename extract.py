#!/usr/bin/env python3

import os
import csv
import sys
import gzip
import json
import warcio
import logging
import datetime
import requests
import urllib.parse
import urllib.request

from pathlib import Path
from jmespath import search as q
from contextlib import contextmanager

archives_json = os.path.join(os.path.dirname(__file__), 'archives.json')
archive_hosts = json.load(open(archives_json))

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S',
    stream=sys.stdout
)
 
def main():
    if len(sys.argv) != 2: 
        sys.exit("usage: extract.py <wat_url>")
    wat_url = sys.argv[1]
    process_wat(wat_url)

def process_wat(wat_url):
    logging.info('processing wat %s', wat_url)
    start = datetime.datetime.now()

    path = localize(wat_url)

    logging.info('extracting from wat %s', path)
    count = 0
    with open_csv(wat_url) as out:
        for record in warcio.ArchiveIterator(open(path, 'rb')):
            if record.rec_headers.get_header('Content-Type') == "application/json":
                for link in extract_archive_links(record):
                    count += 1
                    out.writerow(link)

    os.remove(path)
    logging.info('removed wat %s', path)
    elapsed = datetime.datetime.now() - start
    logging.info('finished processing wat %s: %s seconds', wat_url, elapsed.total_seconds())
    return count

def extract_archive_links(record):
    wat = json.loads(record.raw_stream.read())
    source_url = q('Envelope."WARC-Header-Metadata"."WARC-Target-URI"', wat)
    links = q('Envelope."Payload-Metadata"."HTTP-Response-Metadata"."HTML-Metadata".Links', wat) or []

    # get only the links with a url (ignores css href links, etc)
    links = url_links(links)

    # count of all these links
    link_count = len(links)

    # count of all the external links
    ext_link_count = len(ext_url_links(source_url, links))

    for link in links:
        url = link.get('url')

        # only return links to a known web archive
        archive_service = get_archive_service(url)
        if not archive_service:
            continue

        yield {
            "source_url": source_url,
            "source_host": get_host(source_url),
            "source_archive_service": get_archive_service(source_url),
            "archive_url": url,
            "archive_service": archive_service,
            "link_text": link.get('text'),
            "path": link.get('path'),
            "link_count": link_count,
            "ext_link_count": ext_link_count,
            "warc": q('Container.Filename', wat),
            "offset": q('Container.Offset', wat),
            "inflated_length": q('Container."Gzip-Metadata"."Inflated-Length"', wat),
            "deflated_length": q('Container."Gzip-Metadata"."Deflate-Length"', wat),
            "date": q('Envelope."WARC-Header-Metadata"."WARC-Date"', wat),
        }

def get_archive_service(url):
    return archive_hosts.get(get_host(url))

def get_host(url):
    try:
        u = urllib.parse.urlparse(url)
        return u.netloc
    except Exception as e:
        logging.error('unable to parse url %s: %s', url, e)
        return None

@contextmanager
def open_csv(wat_url):
    path = csv_path(wat_url)
    logging.info('writing to csv %s', path)

    if not path.parent.exists():
        path.parent.mkdir(parents=True)

    fh = path.open('w')

    cols = [
        "date",
        "source_url",
        "source_host",
        "source_archive_service",
        "archive_url",
        "archive_service",
        "link_text",
        "path",
        "link_count",
        "ext_link_count",
        "warc",
        "offset",
        "inflated_length",
        "deflated_length"
    ]

    out = csv.DictWriter(fh, fieldnames=cols)
    out.writeheader()

    try:
        yield out
    finally:
        fh.close()

def localize(url):
    logging.info('downloading %s', url)
    path, headers = urllib.request.urlretrieve(url)
    logging.info('downloaded %s to %s', url, path)
    return path

def url_links(links):
    return list(filter(lambda l: 'url' in l, links))

def ext_url_links(src_url, links):
    result = []
    src_host = get_host(src_url)
    for l in links:
        dst_host = get_host(l['url'])
        if src_host and dst_host and src_host != dst_host:
            result.append(l)
    return result

def csv_path(wat_url):
    return Path(wat_url.replace('https://commoncrawl.s3.amazonaws.com/crawl-data/', 'data/').replace('.warc.wat.gz', '.csv'))

if __name__ == "__main__":
    main()
