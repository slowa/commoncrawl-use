#!/usr/bin/env python3

import os
import rq
import sys
import gzip
import optparse

from redis import Redis
from rq import Queue, Retry
from extract import localize, process_wat, csv_path

def main(snap_id, dryrun=False, overwrite=False):
    q = Queue(connection=Redis(), default_timeout=1200)
    for wat_url in get_wats(snap_id):
        csv_file = csv_path(wat_url)

        # don't overwrite existing csv data unless told to
        if csv_file.exists() and not overwrite:
            print('skipping wat for existing csv {}'.format(csv_file))
            continue

        if dryrun:
            print('would enqueue {}'.format(wat_url))
        else:
            print('enqueuing {}'.format(wat_url))
            q.enqueue(process_wat, wat_url, retry=Retry(max=10, interval=60)) 

def get_wats(snap_id):
    url = "https://commoncrawl.s3.amazonaws.com/crawl-data/" + snap_id + "/wat.paths.gz"
    path = localize(url)
    for line in gzip.open(path, 'rt'):
        line = line.strip()
        url = 'https://commoncrawl.s3.amazonaws.com/' + line
        yield url
    os.remove(path)

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option('--dryrun', action='store_true', help='Only print what would be queued')
    parser.add_option('--overwrite', action='store_true', help='Overwrite existing CSV data for snapshot.')
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error("Please supply one <snapshot_id>")

    main(args[0], dryrun=options.dryrun, overwrite=options.overwrite)
