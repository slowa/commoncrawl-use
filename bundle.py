#!/usr/bin/env python

import os
import sys
import pathlib

if len(sys.argv) != 2:
    sys.exit('usage: bundle.py <snapshot_id>')

snapshot_id = sys.argv[1]
snapshot_dir = pathlib.Path('data') / snapshot_id

if not snapshot_dir.exists():
    sys.exit('no local data available for snapshot id: {}'.format(snapshot_id))

# discover all the CSV files and write them all to out but only write the header once

output_csv = pathlib.Path('data') / (snapshot_id + '.csv')
wrote_header = False

with output_csv.open('w') as out:
    for dirpath, dirnames, filenames in os.walk(snapshot_dir):
        for filename in filenames:
            if filename.endswith('.csv'):
                input_csv = pathlib.Path(dirpath) / filename
                count = 0
                for line in input_csv.open():
                    if count == 0 and not wrote_header:
                        out.write(line)
                        wrote_header = True
                    elif count > 0:
                        out.write(line)
                    count += 1

print('wrote {}'.format(output_csv))
